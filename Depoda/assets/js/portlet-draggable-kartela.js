var PortletDraggable = function () {

    return {
        init: function () {
            if (!jQuery().sortable) {
                return;
            }

            $("#sortable_portlets").sortable({
                connectWith: ".portlet",
                items: ".portlet", 
                opacity: 0.8,
                handle : '.portlet-title',
                coneHelperSize: true,
                placeholder: 'portlet-sortable-placeholder',
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                tolerance: "pointer",
                forcePlaceholderSize: !0,
                helper: "clone",
                cancel: ".portlet-sortable-empty, .portlet-fullscreen", // cancel dragging if portlet is in fullscreen mode
                revert: 250, // animation in milliseconds
                update: function(b, c) {
                    if (c.item.prev().hasClass("portlet-sortable-empty")) {
                        c.item.prev().before(c.item);
                    }

                    $('#sortable_portlets').children('div.portlet').each(function () {
                        $(this).attr("data-order", $(this).index());

                        var formData = new FormData();
                        formData.append('index', $(this).attr("data-order"));
                        formData.append('id', $(this).attr("data-kartelaid"));


                        $.ajax({
                            url: "/admin/kartela/updateKartelaIndex",
                            type: "POST",
                            processData: false,
                            contentType: false,
                            data: formData
                        });

                        
                    });

                    
                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    PortletDraggable.init();
});