var Id = window.location.pathname.split('/')[4];

var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };


    return {
        //main function to initiate the module
        init: function () {

            $('#nestable_list_menu').nestable({
                group: 1
            })
            .on('change', updateOutput);
            updateOutput($('#nestable_list_menu').data('output', $('#nestable_list_menu_output')));
        },
        update: function () {
            updateOutput($('#nestable_list_menu').data('output', $('#nestable_list_menu_output')));
        }

    };

}();

jQuery(document).ready(function () {
    populate();
    UINestable.init();
});


$(document).on('click', '#saveButton', function () {
    update();
});

function populate() {
    $("#saveButton").text("Kaydet");
    $("#ulPages").html("");
    $(".dd-list").html("");
    var formData = new FormData();
    //formData.append("lang", lang);
    $.ajax({
        url: "/menus/getSubMenu/" + Id,
        type: "POST",
        processData: false,
        contentType: false,
        data: formData
    }).done(function (data) {

        jQuery.each(data.pages, function (i, val) {
            $("#ulPages").append(addPageItem(val.id, val.title, val.permaLink));
        });

        jQuery.each(JSON.parse(data.jsonData), function (index, item) {
            $('#nestable_list_menu > .dd-list').append(buildItem(item));
        });

        $("input[name='menuName']").val(data.pageTitle);
        UINestable.update();
    });
}

function update() {

    $("#saveButton").text("Kaydediliyor");

    var formData = new FormData();
    formData.append("id", Id);
    formData.append("jsonData", $("#nestable_list_menu_output").val());
    formData.append("title", $("input[name='menuName']").val())
    $.ajax({
        url: "/menus/updateSubMenus",
        type: "POST",
        processData: false,
        contentType: false,
        data: formData
    }).done(function (data) {
        $("#saveButton").text("Kaydedildi");
        UINestable.update();
    });

    $("#saveButton").text("Kaydet");
}

function buildItem(item) {

    var html = "<li class='dd-item dd3-item' data-id='" + item.id + "'>" +
              "<div class='dd-handle dd3-handle'> </div>" +
              "<i class='fa fa-remove dd4-handle'></i>" +
              "<div class='dd3-content'>" + item.id.split("*|*")[1] + "</div>";

    if (item.children) {
        html += "<ol class='dd-list'>";
        $.each(item.children, function (index, sub) {
            html += buildItem(sub);
        });
        html += "</ol>";

    }

    html += "</li>";

    return html;
}

function addPageItem(value, text, permalink) {
    return "<ul class='aPage" + value + "'>" +
                "<li class='mt-list-item'>" +
                "<div class='list-icon-container'>" +
                    "<i class='fa fa-plus' data-value='" + value + "' data-text='" + text + "' data-permalink='" + permalink + "' aria-hidden='true'></i>" +
                "</div>" +
                "<div class='list-item-content'>" +
                    "<h3>" +
                    "<a href='javascript:;' >" + text + "</a>" +
                    "</h3>" +
                "</div>" +
                "</li>" +
            "</ul>";
}

function addNestablePageItem(value, text, permalink) {
    return "<li class='dd-item dd3-item' data-id='" + value + "*|*" + text + "*|*" + permalink + "'>" +
                "<div class='dd-handle dd3-handle'> </div>" +
                "<i class='fa fa-remove dd4-handle'></i>" +
                "<div class='dd3-content'>" + text + "</div>" +
            "</li>";
}

$(document).on('click', '.list-icon-container .fa-plus', function () {
    $("#nestable_list_menu > .dd-list").append(addNestablePageItem($(this).data("value"), $(this).data("text"), $(this).data("permalink")));
    UINestable.update();
});

$(document).on('click', '.dd-list .fa-remove', function () {
    $(this).closest('li').remove();
    UINestable.update();
});

$(document).on('click', '.btn-group .btn-default', function () {
    populate($(this).data("value"));
});

$(document).on('click', '.btn-add', function () {
    
    $("#nestable_list_menu > .dd-list").append(addNestablePageItem("-1", $("input[name='pageName']").val(), $("input[name='pageURL']").val()));
    UINestable.update();
    $("input[name='pageName']").val("");
    $("input[name='pageURL']").val("");

});