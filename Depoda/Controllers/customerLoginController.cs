﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Microsoft.Owin.Security;
using System.Web.Security;
using System.Web.Helpers;
using System.Net.Mail;
using Depoda;
using Depoda.Models;

namespace Depoda.Controllers
{
    public class customerLoginController : Controller
    {
        private DepodaEntities db = new DepodaEntities();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("order");
            }
            else
                return View("login");
        }

        public ActionResult order()
        {
            var warehouses = db.warehouse.ToList();
            return View("order",warehouses);
        }
    
        public ActionResult orderDetail(int id)
        {
            orderDetail order = new Models.orderDetail();
            order.warehouseId = id;
             var warehouse=db.warehouse.Find(id);
            order.prodPrice =Convert.ToInt32(warehouse.product.price);
            order.productId = Convert.ToInt32(warehouse.product.id);
            order.quantity = Convert.ToInt32(warehouse.quantity);
            return View("orderDetail", order);
        }

        public ActionResult setOrder(int productId, int warehouseId,string amount,int orderNumber)
        {
            var warehouse = db.warehouse.Find(warehouseId);
            var purchase = new purchase();
            purchase.warehouse_id = warehouseId;
            purchase.quantity = orderNumber.ToString();
            purchase.product_id = productId;
            purchase.total_price = amount.Split(' ')[0];
            purchase.date = DateTime.Now;
            purchase.customer_id = Convert.ToInt32(HttpContext.Session["customerId"].ToString());
            warehouse.quantity = (Convert.ToInt32(warehouse.quantity) - orderNumber).ToString();
            db.purchase.Add(purchase);
            db.SaveChanges();
            return View("order", db.warehouse.ToList());
        }

        public ActionResult login(string phoneNumber, bool remember = false)
        {
            var r = db.customer.SingleOrDefault(p => p.phone_nr == phoneNumber);

            if (r == null)
                return View("login");
            else
            {
                FormsAuthentication.SetAuthCookie(r.name, remember);
                HttpContext.Session["customerId"] = r.id;




                return RedirectToAction("order");
            }
        }

        public ActionResult logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("index");
        }
    }


}