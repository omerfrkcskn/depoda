﻿using Depoda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Depoda.Controllers
{
    public class usersController : Controller
    {

        DepodaEntities db = new DepodaEntities();


        public ActionResult insertUpdate(int id, string name, string password, string email)
        {
            User p;
            if (id == 0)
            {
                p = new User
                {
                    name = name,
                    password = password,
                    email = email,
                };



                db.User.Add(p);
            }
            else
            {
                p = db.User.SingleOrDefault(a => a.id == id);
                p.name = name;
                p.password = password;
                p.email = email;
                
                db.Entry(p).State = System.Data.Entity.EntityState.Modified;


            }
            db.SaveChanges();


            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult manage(int? id)
        {
            usersViewModel uVM;

            if (id == null)
            {
                uVM = new usersViewModel
                {
                    user = null,
                };
            }
            else
            {
                uVM = new usersViewModel
                {
                    user = db.User.SingleOrDefault(a => a.id == id),
                };
            }


            return View("manage", uVM);
        }

        public ActionResult list()
        {
            return View(db.User.ToList());
        }

        public ActionResult logout()
        {
            return RedirectToAction("list");
        }


        public ActionResult delete(int id)
        {
            return View("delete", db.User.SingleOrDefault(a => a.id == id));
        }

        public ActionResult deleteConfirmed(int id)
        {
            db.User.Remove(db.User.SingleOrDefault(a => a.id == id));
            db.SaveChanges();

            return RedirectToAction("list");
        }
    }
}