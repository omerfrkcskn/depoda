﻿using Depoda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Depoda.Controllers
{
    public class customerController : Controller
    {

        DepodaEntities db = new DepodaEntities();

        [HttpPost]
        public ActionResult insertUpdate(int id, string name, string phoneNumber, string customerNo, string address)
        {
            customer p;
            if (id == 0)
            {
                p = new customer
                {
                    name = name,
                    phone_nr=phoneNumber,
                    customer_nr=customerNo,
                    adress=address
                };



                db.customer.Add(p);
            }
            else
            {
                p = db.customer.SingleOrDefault(a => a.id == id);
                p.name = name;
                p.phone_nr = phoneNumber;
                p.customer_nr = customerNo;
                p.adress = address;
                db.Entry(p).State = System.Data.Entity.EntityState.Modified;


            }
            db.SaveChanges();


            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult manage(int? id)
        {
            customerViewModel uVM;

            if (id == null)
            {
                uVM = new customerViewModel
                {
                    customer = null,
                };
            }
            else
            {
                uVM = new customerViewModel
                {
                    customer = db.customer.SingleOrDefault(a => a.id == id),
                };
            }


            return View("manage", uVM);
        }

        public ActionResult list()
        {
            return View(db.customer.ToList());
        }

        public ActionResult logout()
        {
            return RedirectToAction("list");
        }


        public ActionResult delete(int id)
        {
            return View("delete", db.customer.SingleOrDefault(a => a.id == id));
        }

        public ActionResult deleteConfirmed(int id)
        {
            db.customer.Remove(db.customer.SingleOrDefault(a => a.id == id));
            db.SaveChanges();

            return RedirectToAction("list");
        }
    }
}