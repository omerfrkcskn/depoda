﻿using Depoda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Depoda.Controllers
{
    public class purchaseController : Controller
    {

        DepodaEntities db = new DepodaEntities();



        public ActionResult list()
        {
            var purchaseList = new List<purchaseViewModel>();
            var purchase = db.purchase.OrderByDescending(res => res.date).ToList();
            foreach(var item in purchase)
            {
                var customer = db.customer.Find(item.customer_id);
                var purchaseItem = new purchaseViewModel();
                purchaseItem.purchaseItem = item;
                purchaseItem.customerName = customer.name;
                purchaseList.Add(purchaseItem);
            }
            return View(purchaseList);
        }

    }
}