﻿using Depoda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Depoda.Controllers
{
    public class productController : Controller
    {

        DepodaEntities db = new DepodaEntities();

        [HttpPost]
        public ActionResult insertUpdate(int id, string name, int product_code_nr, string price, string type_number)
        {
            product p;
            if (id == 0)
            {
                p = new product
                {
                    name = name,
                    product_code_nr = product_code_nr,
                    type_number= type_number,
                    price = price

                };



                db.product.Add(p);
            }
            else
            {
                p = db.product.SingleOrDefault(a => a.id == id);
                p.name = name;
                p.product_code_nr = product_code_nr;
                p.type_number = type_number;
                p.price = price;
                db.Entry(p).State = System.Data.Entity.EntityState.Modified;


            }
            db.SaveChanges();


            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult manage(int? id)
        {
            productViewModel uVM;

            if (id == null)
            {
                uVM = new productViewModel
                {
                    product = null,
                };
            }
            else
            {
                uVM = new productViewModel
                {
                    product = db.product.SingleOrDefault(a => a.id == id),
                };
            }


            return View("manage", uVM);
        }

        public ActionResult list()
        {
            return View(db.product.ToList());
        }

        public ActionResult logout()
        {
            return RedirectToAction("list");
        }


        public ActionResult delete(int id)
        {
            return View("delete", db.product.SingleOrDefault(a => a.id == id));
        }

        public ActionResult deleteConfirmed(int id)
        {
            db.product.Remove(db.product.SingleOrDefault(a => a.id == id));
            db.SaveChanges();

            return RedirectToAction("list");
        }
    }
}