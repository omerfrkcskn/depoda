﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Microsoft.Owin.Security;
using System.Web.Security;
using System.Web.Helpers;
using System.Net.Mail;
using Depoda;
using Depoda.Models;

namespace Depoda.Controllers
{
    public class HomeController : Controller
    {
        private DepodaEntities db = new DepodaEntities();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("welcome");
            else
                return View("login");
        }

        public ActionResult welcome()
        {
            return View("welcome");
        }

        public ActionResult login(string email, string password, bool remember = false)
        {
            var r = db.User.SingleOrDefault(p => p.email == email && p.password == password);

            if (r == null)
                return View("login");
            else
            {

                FormsAuthentication.SetAuthCookie(r.name, remember);




                return RedirectToAction("welcome");
            }
        }

        public ActionResult logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("index");
        }
    }


}