﻿using Depoda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Depoda.Controllers
{
    public class warehouseController : Controller
    {

        DepodaEntities db = new DepodaEntities();

        [HttpPost]
        public ActionResult insertUpdate(int id, int product_code_nr, string warehouse_nr, string quantity)
        {
            warehouse p;
            if (id == 0)
            {
                p = new warehouse
                {
                    product_code_nr = product_code_nr,
                    warehouse_nr = warehouse_nr,
                    quantity = quantity

                };



                db.warehouse.Add(p);
            }
            else
            {
                p = db.warehouse.SingleOrDefault(a => a.id == id);
                p.product_code_nr = product_code_nr;
                p.warehouse_nr = warehouse_nr;
                p.quantity = quantity;
                db.Entry(p).State = System.Data.Entity.EntityState.Modified;


            }
            db.SaveChanges();


            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult manage(int? id)
        {
            warehouseViewModel uVM;

            if (id == null)
            {
                uVM = new warehouseViewModel
                {
                    warehouse = null,
                };
            }
            else
            {
                uVM = new warehouseViewModel
                {
                    warehouse = db.warehouse.SingleOrDefault(a => a.id == id),
                };
            }


            return View("manage", uVM);
        }

        public ActionResult list()
        {
            return View(db.warehouse.ToList());
        }

        public ActionResult logout()
        {
            return RedirectToAction("list");
        }


        public ActionResult delete(int id)
        {
            return View("delete", db.warehouse.SingleOrDefault(a => a.id == id));
        }

        public ActionResult deleteConfirmed(int id)
        {
            db.warehouse.Remove(db.warehouse.SingleOrDefault(a => a.id == id));
            db.SaveChanges();

            return RedirectToAction("list");
        }
    }
}