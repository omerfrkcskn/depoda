﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Depoda.Models
{
    public class orderDetail
    {
        public int prodPrice { get; set; }
        public int warehouseId { get; set; }

        public int productId { get; set; }

        public int quantity { get; set; }

    }
}